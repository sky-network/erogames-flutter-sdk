import 'dart:async';

import 'package:flutter/services.dart';

class ErogamesAnalytics {
  static const MethodChannel _methodChannel =
      const MethodChannel('com.erogames.flutter/analytics');
  static const _Param param = _Param();

  static Future<void> init(String clientId) async {
    var args = <String, dynamic>{'client_id': clientId};
    await _methodChannel.invokeMethod('init', args);
  }

  /// Send an event to the server asynchronously.
  /// The event will be added to the queue immediately.
  /// But sending may be delayed if, for example, there is no internet connection, etc.
  /// A user does not have to worry about it because the event will be stored until it is sent.
  ///
  /// See also: https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/analytics
  static Future<void> logEvent(String event,
      {Map<String, String> params = const {}}) async {
    var args = <String, dynamic>{'event': event, 'params': params};
    await _methodChannel.invokeMethod('log_event', args);
  }
}

class _Param {
  const _Param();

  final String eaCategory = "ea_category";
  final String eaSourceId = "ea_source_id";
}
