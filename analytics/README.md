# Erogames Analytics Plugin

## Integration into the project

To add the plugin to the project add the reference to the `pubspec.yaml` file:

```dart
...
dependencies:
  erogames_analytics:
    git:
      url: git@gitlab.com:sky-network/erogames-flutter-sdk.git
      path: analytics
      ref: analytics-v<x.y.z>
...
```
```dart
import 'package:erogames_analytics/erogames_analytics.dart';
```

### Init
```dart
ErogamesAnalytics.init("your_client_id");
```

### Log event
To override data used by ErogamesAnalytics internally, add `ea_` prefix to the param name.  
To add custom data, the param name should not be started with `ea_` prefix.

```dart
ErogamesAnalytics.logEvent("install");
```

Log event with a payload:
```dart
Map<String, String> params = {
  "ea_category": "banners"
};
ErogamesAnalytics.logEvent(install, params);
```

### How to debug:
```shell
adb logcat -s EventWorker -s TrackProviderUtil
```
