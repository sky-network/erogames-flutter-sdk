import 'package:erogames_analytics/erogames_analytics.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    ErogamesAnalytics.init("pG8uvkv4nPzsTFWoW_kJnqzdOxa0Us576zastDoROaE");
    super.initState();
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Erogames Analytics (Flutter)'),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _eventButton1(),
              _eventButton2(),
              _eventButton3(),
            ],
          ),
        ),
      );

  Widget _eventButton1() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Text('Send event 1'),
              onPressed: () => _sendEvent1(),
            ),
          ],
        ),
      );

  Widget _eventButton2() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Text('Send event 2'),
              onPressed: () => _sendEvent2(),
            ),
          ],
        ),
      );

  Widget _eventButton3() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Text('Send event 3'),
              onPressed: () => _sendEvent3(),
            ),
          ],
        ),
      );

  void _sendEvent1() => ErogamesAnalytics.logEvent("install");

  void _sendEvent2() {
    Map<String, String> params = {
      ErogamesAnalytics.param.eaCategory: "test_category_2"
    };
    ErogamesAnalytics.logEvent("click", params: params);
  }

  void _sendEvent3() {
    Map<String, String> params = {
      ErogamesAnalytics.param.eaCategory: "test_category_3",
      "test_param": "test_value"
    };
    ErogamesAnalytics.logEvent("install", params: params);
  }
}
