package com.erogames.flutter.analytics

import android.content.Context
import androidx.annotation.NonNull
import com.erogames.analytics.ErogamesAnalytics
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** AnalyticsPlugin */
class AnalyticsPlugin : FlutterPlugin, MethodCallHandler {

    private lateinit var context: Context
    private lateinit var methodChannel: MethodChannel

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        context = flutterPluginBinding.applicationContext
        methodChannel = MethodChannel(
            flutterPluginBinding.binaryMessenger,
            METHOD_CHANNEL_NAME
        )
        methodChannel.setMethodCallHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "init" -> {
                val clientId = call.argument<String>("client_id") ?: ""
                ErogamesAnalytics.init(context, clientId)
                result.success(true)
            }
            "log_event" -> {
                val event = call.argument<String>("event") ?: ""
                val params = call.argument<Map<String, String>>("params") ?: mapOf()
                ErogamesAnalytics.logEvent(context, event, params)
                result.success(true)
            }
            else -> result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel.setMethodCallHandler(null)
    }

    companion object {
        private const val METHOD_CHANNEL_NAME: String = "com.erogames.flutter/analytics"
    }
}
