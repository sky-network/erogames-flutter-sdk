import 'dart:async';

import 'package:erogames_auth/auth.dart';
import 'package:erogames_auth/model/user.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  User? _user;
  Locale? _locale;
  int counter = 0;

  @override
  void initState() {
    super.initState();
    initEroAuth();
  }

  Future<void> initEroAuth() async {
    ErogamesAuth.init("pG8uvkv4nPzsTFWoW_kJnqzdOxa0Us576zastDoROaE");
    ErogamesAuth.getUser().then((user) {
      debugPrint('get_user: ${user?.toJson().toString()}');
      if (!mounted) return;
      setState(() => _user = user);
    });
    ErogamesAuth.getToken().then(
        (token) => debugPrint('get_token: ${token?.toJson().toString()}'));

    ErogamesAuth.onAuth((user, error) {
      debugPrint('auth_error: $error');
      if (!mounted) return;
      setState(() => _user = user);
    });
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Erogames Auth (Flutter)'),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(_user?.toJson().toString() ?? ''),
              )),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Visibility(
                    visible: _user == null,
                    child: DropdownButton<String>(
                      value: _locale?.languageCode,
                      hint: const Text('Select a language'),
                      items: <String>['en', 'fr', 'zh', 'ja']
                          .map((value) => DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              ))
                          .toList(),
                      onChanged: (String? value) {
                        setState(() => _locale = Locale(value ?? 'en'));
                      },
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            child: const Text('Reload User'),
                            onPressed: () {
                              ErogamesAuth.reloadUser().then((_) {
                                ErogamesAuth.getUser().then((user) => debugPrint(
                                    'reload_user: ${user?.toJson().toString()}'));
                              }).catchError(
                                  (error) => debugPrint(error.toString()));
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            child: const Text('Refresh Token'),
                            onPressed: () {
                              ErogamesAuth.refreshToken().then((_) {
                                ErogamesAuth.getToken().then((token) => debugPrint(
                                    'refresh_token: ${token?.toJson().toString()}'));
                              }).catchError(
                                  (error) => debugPrint(error.toString()));
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            child: const Text('Test'),
                            onPressed: () {
                              counter++;
                              switch (counter) {
                                case 1:
                                  _loadWhitelabel();
                                  break;
                              }
                              if (counter >= 1) counter = 0;
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Visibility(
                        visible: _user == null,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                child: const Text('Login'),
                                onPressed: () =>
                                    ErogamesAuth.login(locale: _locale),
                              ),
                              ElevatedButton(
                                child: const Text('Signup'),
                                onPressed: () =>
                                    ErogamesAuth.signup(locale: _locale),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _user != null,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                child: const Text('Log out'),
                                onPressed: () =>
                                    ErogamesAuth.logout(webLogout: false),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  void _loadWhitelabel() {
    ErogamesAuth.loadWhitelabel().then((value) => ErogamesAuth.getWhitelabel()
        .then((value) =>
            debugPrint("OnWhitelabel: ${value?.buyErogoldUrl["en"] ?? ''}")));
  }
}
