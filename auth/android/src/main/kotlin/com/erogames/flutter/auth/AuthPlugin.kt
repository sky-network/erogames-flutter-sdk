package com.erogames.flutter.auth

import android.app.Activity
import android.content.Intent
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.OnResult
import com.erogames.auth.model.PaymentInfo
import com.erogames.auth.model.QuestData
import com.erogames.auth.model.User
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import kotlinx.serialization.encodeToString
import java.util.*

/** AuthPlugin */
class AuthPlugin : FlutterPlugin, MethodCallHandler,
    PluginRegistry.ActivityResultListener,
    EventChannel.StreamHandler, ActivityAware {

    private lateinit var activity: Activity
    private lateinit var methodChannel: MethodChannel
    private lateinit var eventChannel: EventChannel
    private var authCodeListener: AuthCodeCallback? = null

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel = MethodChannel(
            flutterPluginBinding.binaryMessenger,
            METHOD_CHANNEL_NAME
        )
        methodChannel.setMethodCallHandler(this)

        eventChannel = EventChannel(
            flutterPluginBinding.binaryMessenger,
            EVENT_CHANNEL_NAME
        )
        eventChannel.setStreamHandler(this)
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        val locale = call.argument<String>("locale")
        return when (call.method) {
            "init" -> {
                val clientId = call.argument<String>("client_id") ?: ""
                ErogamesAuth.init(activity, clientId)
                result.success(true)
            }
            "signup" -> {
                ErogamesAuth.signup(activity, Locale(locale ?: "en"))
                result.success(true)
            }
            "login" -> {
                ErogamesAuth.login(activity, Locale(locale ?: "en"))
                result.success(true)
            }
            "logout" -> {
                val webLogout = call.argument<Boolean>("web_logout")
                ErogamesAuth.logout(activity, webLogout ?: false)
                authCodeListener?.onSuccess(null)
                result.success(true)
            }
            "get_user" -> {
                val user = ErogamesAuth.getUser(activity)
                var jsonResp: String? = null
                user?.let { jsonResp = InjectorUtil.myJson.encodeToString(user) }
                result.success(jsonResp)
            }
            "reload_user" -> {
                ErogamesAuth.reloadUser(activity, object : OnResult<Void?> {
                    override fun onSuccess(data: Void?) = result.success(null)
                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "reload_user")
                })
            }
            "get_token" -> {
                val token = ErogamesAuth.getToken(activity)
                var jsonResp: String? = null
                token?.let { jsonResp = InjectorUtil.myJson.encodeToString(token) }
                result.success(jsonResp)
            }
            "refresh_token" -> {
                ErogamesAuth.refreshToken(activity, object : OnResult<Void?> {
                    override fun onSuccess(data: Void?) = result.success(null)
                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "refresh_token")
                })
            }
            "get_whitelabel" -> {
                val whitelabel = ErogamesAuth.getWhitelabel(activity)
                var jsonResp: String? = null
                whitelabel?.let {
                    jsonResp = InjectorUtil.myJson.encodeToString(whitelabel)
                }
                result.success(jsonResp)
            }
            "load_whitelabel" -> {
                ErogamesAuth.loadWhitelabel(activity, object : OnResult<Void?> {
                    override fun onSuccess(data: Void?) = result.success(null)
                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "load_whitelabel")
                })
            }
            "proceed_payment" -> {
                val paymentId = call.argument<String>("payment_id") ?: ""
                val amount = call.argument<Int>("amount") ?: 0

                ErogamesAuth.proceedPayment(activity, paymentId, amount, object : OnResult<Void?> {
                    override fun onSuccess(data: Void?) = result.success(null)
                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "proceed_payment")
                })
            }
            "load_current_quest" -> {
                ErogamesAuth.loadCurrentQuest(activity, object : OnResult<QuestData> {
                    override fun onSuccess(data: QuestData) =
                        result.success(InjectorUtil.myJson.encodeToString(data))

                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "load_current_quest")
                })
            }
            "load_payment_info" -> {
                val paymentId = call.argument<String>("payment_id") ?: ""
                ErogamesAuth.loadPaymentInfo(activity, paymentId, object : OnResult<PaymentInfo> {
                    override fun onSuccess(data: PaymentInfo) =
                        result.success(InjectorUtil.myJson.encodeToString(data))

                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "load_payment_info")
                })
            }
            "has_update" -> {
                ErogamesAuth.hasUpdate(activity, object : OnResult<Boolean> {
                    override fun onSuccess(data: Boolean) =
                        result.success(data)

                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "has_update")
                })
            }
            "open_in_app_store" -> {
                ErogamesAuth.openInAppStore(activity, object : OnResult<Boolean> {
                    override fun onSuccess(data: Boolean) =
                        result.success(data)

                    override fun onFailure(t: Throwable?) =
                        result.error("-1", t?.message, "open_in_app_store")
                })
            }
            else -> result.notImplemented()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ErogamesAuth.REQUEST_CODE_AUTH) {
                // Optionally, get user: ErogamesAuth.getUser(activity)
                val user: User? = data?.getParcelableExtra(ErogamesAuth.EXTRA_USER)
                val errorMessage: String =
                    (data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)) ?: "Error"

                when {
                    user != null -> authCodeListener?.onSuccess(user)
                    else -> authCodeListener?.onError(errorMessage)
                }
            }
        }
        return false
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink) {
        setAuthCodeCallback(object : AuthCodeCallback {
            override fun onSuccess(user: User?) {
                var jsonResp: String? = null
                user?.let { jsonResp = InjectorUtil.myJson.encodeToString(user) }
                events.success(jsonResp)
            }

            override fun onError(errMessage: String) =
                events.error("-1", errMessage, null)
        })
    }

    override fun onCancel(arguments: Any?) = Unit

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel.setMethodCallHandler(null)
        eventChannel.setStreamHandler(null)
    }

    private fun setAuthCodeCallback(authCodeCallback: AuthCodeCallback) {
        authCodeListener = authCodeCallback
    }

    internal interface AuthCodeCallback {
        fun onSuccess(user: User?)
        fun onError(errMessage: String)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        binding.addActivityResultListener(this)
        activity = binding.activity
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        binding.addActivityResultListener(this)
        activity = binding.activity
    }

    override fun onDetachedFromActivity() = Unit

    override fun onDetachedFromActivityForConfigChanges() = onDetachedFromActivity()

    companion object {
        // private const val TAG = "AuthPlugin"
        private const val METHOD_CHANNEL_NAME: String = "com.erogames.flutter/auth"
        private const val EVENT_CHANNEL_NAME: String = "com.erogames.flutter/auth_status"
    }
}
