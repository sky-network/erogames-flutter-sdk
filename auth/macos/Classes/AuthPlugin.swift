import Cocoa
import FlutterMacOS

public class AuthPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "com.erogames.flutter/auth", binaryMessenger: registrar.messenger)
        let instance = AuthPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let text : String = getArg(key: "text", call: call) as? String ?? ""
        switch call.method {
        case "showSpinner":
            showHideSpinner(show: true, text: text)
            result(true)
        case "hideSpinner":
            showHideSpinner(show: false, text: text)
            result(true)
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    private func getArg(key: String, call: FlutterMethodCall)-> Any? {
        guard let args = call.arguments as? Dictionary<String, Any> else {
            return nil
        }
        return args[key]
    }

    private func showHideSpinner(show: Bool, text: String) {
        if (show) {
            let view = NSApplication.shared.mainWindow!.contentView
            ProgressHUD.setContainerView(view)
            ProgressHUD.setDefaultMaskType(ProgressHUDMaskType.black)
            ProgressHUD.setDefaultPosition(.center)
            ProgressHUD.setSpinnerSize(30.0)
            ProgressHUD.setDefaultStyle(.custom(foreground: .red, backgroud: .white))
            ProgressHUD.show(withStatus: text)
        } else {
            ProgressHUD.dismiss()
        }
    }
}
