import 'package:erogames_auth/repository/repository.dart';
import 'package:erogames_auth/util/auth_util.dart';
import 'package:erogames_auth/util/injector_util.dart';
import 'package:erogames_auth/util/pkce.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;
import 'package:protocol_handler/protocol_handler.dart';

import 'auth_platform_interface.dart';
import 'model/token.dart';
import 'model/user.dart';
import 'model/whitelabel.dart';

/// An implementation of [AuthPlatform] that uses method channels.
class MethodChannelAuth extends AuthPlatform with ProtocolListener {
  final _methodChannel = const MethodChannel('com.erogames.flutter/auth');
  final _eventChannel = const EventChannel('com.erogames.flutter/auth_status');

  AuthCallback? _authCallback;

  @override
  Future<void> onAuth(AuthCallback authCallback) async {
    if (Platform.isMacOS || Platform.isWindows) {
      _authCallback = authCallback;
    } else {
      _eventChannel.receiveBroadcastStream().listen((user) {
        if (user != null && user != 'null') {
          authCallback(User.fromString(user), null);
        } else {
          authCallback(null, null);
        }
      }, onError: (error) {
        var errMessage = (error is PlatformException) ? error.message : error;
        authCallback(null, errMessage.toString());
      });
    }
  }

  @override
  Future<void> init(String clientId, {String? packageName}) async {
    if (Platform.isWindows) {
      assert(packageName != null);
      await InjectorUtil().provideRepository().setPackageName(packageName!);
    }

    if (Platform.isMacOS || Platform.isWindows) {
      await InjectorUtil().provideRepository().setClientId(clientId);
      protocolHandler.addListener(this);
      await protocolHandler.register((await AuthUtil.getPackageName()));
    } else {
      var args = <String, dynamic>{'client_id': clientId};
      await _methodChannel.invokeMethod('init', args);
    }
  }

  @override
  void onProtocolUrlReceived(String url) async {
    var uri = Uri.parse(url);
    if (await AuthUtil.isAuthDeepLinkValid(uri)) {
      if (uri.queryParameters.containsKey('code')) {
        _proceedAuth(uri.queryParameters['code']!);
      }
      if (uri.queryParameters.containsKey('logout')) {
        _authCallback?.call(null, null);
      }
    }
  }

  @override
  Future<void> login({Locale? locale}) async {
    if (Platform.isMacOS || Platform.isWindows) {
      _showHideSpinner(true, text: 'Logging in…');
      try {
        Pkce().getCodeVerifier(generateNew: true);
        var repo = InjectorUtil().provideRepository();
        repo.setLocale(locale ?? const Locale('en'));
        var whitelabel = await repo.loadWhitelabel();
        var authUrl = await _buildAuthUrl(whitelabel.authorizeUrl!, false);
        await launchUrl(Uri.parse(authUrl));
      } catch (e) {
        _showHideSpinner(false);
        _authCallback?.call(null, e.toString());
      }
    } else {
      var args = <String, dynamic>{'locale': locale?.languageCode};
      await _methodChannel.invokeMethod('login', args);
    }
  }

  @override
  Future<void> signup({Locale? locale}) async {
    if (Platform.isMacOS || Platform.isWindows) {
      _showHideSpinner(true, text: 'Signing up…');
      try {
        Pkce().getCodeVerifier(generateNew: true);
        var repo = InjectorUtil().provideRepository();
        repo.setLocale(locale ?? const Locale('en'));
        var whitelabel = await repo.loadWhitelabel();
        var authUrl = await _buildAuthUrl(whitelabel.authorizeUrl!, true);
        await launchUrl(Uri.parse(authUrl));
      } catch (e) {
        _showHideSpinner(false);
        _authCallback?.call(null, e.toString());
      }
    } else {
      var args = <String, dynamic>{'locale': locale?.languageCode};
      await _methodChannel.invokeMethod('signup', args);
    }
  }

  @override
  Future<void> logout({bool webLogout = false}) async {
    if (Platform.isMacOS || Platform.isWindows) {
      var repo = InjectorUtil().provideRepository();
      var webLogoutUrl = await _buildWebLogoutUrl();
      await repo.clearLocalData();
      _showHideSpinner(false);
      _authCallback?.call(null, null);
      if (webLogout && webLogoutUrl != null) {
        await launchUrl(Uri.parse(webLogoutUrl));
      }
    } else {
      var args = <String, dynamic>{'web_logout': webLogout};
      await _methodChannel.invokeMethod('logout', args);
    }
  }

  @override
  Future<User?> getUser() async {
    if (Platform.isMacOS || Platform.isWindows) {
      return await InjectorUtil().provideRepository().getUser();
    } else {
      var user = await _methodChannel.invokeMethod('get_user');
      if (user != null && user != 'null') return User.fromString(user);
    }
    return null;
  }

  @override
  Future<void> reloadUser() async {
    if (Platform.isMacOS || Platform.isWindows) {
      await InjectorUtil().provideRepository().reloadUser();
    } else {
      await _methodChannel.invokeMethod<void>('reload_user');
    }
  }

  @override
  Future<Token?> getToken() async {
    if (Platform.isMacOS || Platform.isWindows) {
      return await InjectorUtil().provideRepository().getToken();
    } else {
      var token = await _methodChannel.invokeMethod('get_token');
      if (token != null && token != 'null') return Token.fromString(token);
    }
    return null;
  }

  @override
  Future<void> refreshToken() async {
    if (Platform.isMacOS || Platform.isWindows) {
      await InjectorUtil().provideRepository().refreshToken();
    } else {
      await _methodChannel.invokeMethod<void>('refresh_token');
    }
  }

  @override
  Future<Whitelabel?> getWhitelabel() async {
    if (Platform.isMacOS || Platform.isWindows) {
      return await InjectorUtil().provideRepository().getWhitelabel();
    } else {
      var whitelabelStr = await _methodChannel.invokeMethod('get_whitelabel');
      if (whitelabelStr != null && whitelabelStr != 'null') {
        return Whitelabel.fromString(whitelabelStr);
      }
    }
    return null;
  }

  @override
  Future<void> loadWhitelabel() async {
    if (Platform.isMacOS || Platform.isWindows) {
      await InjectorUtil().provideRepository().loadWhitelabel();
    } else {
      await _methodChannel.invokeMethod<void>('load_whitelabel');
    }
  }

  Future<String> _buildAuthUrl(
      String authorizeUrl, bool forceRegistration) async {
    var codeChallenge = Pkce().generateCodeChallenge(Pkce().getCodeVerifier());
    var repo = InjectorUtil().provideRepository();
    var clientId = await repo.getClientId();
    var locale = await repo.getLocale();
    var redirectUri = await AuthUtil.buildRedirectUri();

    return AuthUtil.buildAuthUrl(authorizeUrl, codeChallenge, redirectUri,
        clientId, locale, forceRegistration.toString());
  }

  Future<String?> _buildWebLogoutUrl() async {
    var repo = InjectorUtil().provideRepository();
    var whitelabel = await repo.getWhitelabel();
    var accessToken = (await repo.getToken())?.accessToken;
    if (whitelabel != null && accessToken != null) {
      return AuthUtil.buildWebLogoutUrl(
          whitelabel.url!, accessToken, await AuthUtil.buildRedirectUri());
    }
    return null;
  }

  Future<void> _proceedAuth(String code) async {
    _showHideSpinner(true, text: 'Logging in…');
    var redirectUri = await AuthUtil.buildRedirectUri();
    Repository repo = InjectorUtil().provideRepository();
    try {
      await repo.loadToken(
          code, 'authorization_code', redirectUri, Pkce().getCodeVerifier());
      await repo.loadUser();
      _authCallback?.call(await repo.getUser(), null);
    } catch (e) {
      _authCallback?.call(null, e.toString());
    } finally {
      _showHideSpinner(false);
    }
  }

  Future<void> _showHideSpinner(bool show, {String text = ''}) async {
    var args = <String, dynamic>{'text': text};
    var method = show ? 'showSpinner' : 'hideSpinner';
    await _methodChannel.invokeMethod(method, args);
  }
}
