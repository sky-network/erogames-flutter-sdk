import 'dart:ui';

import 'auth_platform_interface.dart';
import 'model/token.dart';
import 'model/user.dart';
import 'model/whitelabel.dart';

class ErogamesAuth {
  /// Monitors the authentication state.
  /// [authCallback] will trigger once the user has logged in, signed up, or logged out.
  static Future<void> onAuth(AuthCallback authCallback) async =>
      AuthPlatform.instance.onAuth(authCallback);

  /// ErogamesAuth initialization.
  /// The [packageName] is mandatory for Windows OS.
  /// On other platforms, The [packageName] is ignored.
  static Future<void> init(String clientId, {String? packageName}) async =>
      await AuthPlatform.instance.init(clientId, packageName: packageName);

  /// Starts sign up process. The corresponding "signup" web URL will be opened in the default browser.
  /// The Custom Chrome Tabs can be used if available.
  /// The [locale] is preferable language of the "signup" web page.
  static Future<void> signup({Locale? locale}) async =>
      await AuthPlatform.instance.signup(locale: locale);

  /// Starts sign in process. The corresponding "sign in" web URL will be opened in the default browser.
  /// The Custom Chrome Tabs can be used if available.
  /// The [locale] is preferable language of the "sign in" web page.
  static Future<void> login({Locale? locale}) async =>
      await AuthPlatform.instance.login(locale: locale);

  /// Clear all local authentication data.
  /// If [webLogout] is true then the corresponding "log out" web URL will be opened in the default browser.
  /// In this case, the user will also be logged out from the web site.
  static Future<void> logout({bool webLogout = false}) async =>
      await AuthPlatform.instance.logout(webLogout: webLogout);

  /// Returns the current user data, or null if the user is not logged in.
  static Future<User?> getUser() async => await AuthPlatform.instance.getUser();

  /// Refreshes the current user data.
  static Future<void> reloadUser() async =>
      await AuthPlatform.instance.reloadUser();

  /// Returns the current token, or null if the user is not logged in.
  static Future<Token?> getToken() async =>
      await AuthPlatform.instance.getToken();

  /// Refreshes the current token. For example, can be used if the token is expired.
  static Future<void> refreshToken() async =>
      await AuthPlatform.instance.refreshToken();

  /// Returns [Whitelabel], or null if the user is not logged in.
  static Future<Whitelabel?> getWhitelabel() async =>
      await AuthPlatform.instance.getWhitelabel();

  /// Loads the whitelabel. For example, can be used if the user is not logged in.
  /// To get the loaded whitelabel use [getWhitelabel()]
  static Future<void> loadWhitelabel() async =>
      await AuthPlatform.instance.loadWhitelabel();
}
