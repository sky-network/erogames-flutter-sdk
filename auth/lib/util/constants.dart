class Constants {
  Constants._internal();

  static const List<String> supportedLanguages = ['en', 'fr', 'zh', 'ja', 'es'];
}
