import 'package:erogames_auth/repository/storage.dart';

import '../api/api_service.dart';
import '../repository/repository.dart';

class InjectorUtil {
  Repository? _repo;
  ApiService? _apiService;
  Storage? _storage;

  static final InjectorUtil _instance = InjectorUtil._internal();
  static const whitelabelId =
      String.fromEnvironment('WHITELABEL', defaultValue: 'erogames');

  factory InjectorUtil() => _instance;

  InjectorUtil._internal();

  Repository provideRepository() {
    if (_repo != null) return _repo!;
    var accessKey = 'b63cd11ff4c45e9b20b454f5dd9a93f6';
    _repo = Repository(
        accessKey, whitelabelId, provideApiService(), provideStorage());
    return _repo!;
  }

  ApiService provideApiService() {
    if (_apiService != null) return _apiService!;
    _apiService = ApiService();
    return _apiService!;
  }

  Storage provideStorage() {
    if (_storage != null) return _storage!;
    _storage = Storage();
    return _storage!;
  }
}
