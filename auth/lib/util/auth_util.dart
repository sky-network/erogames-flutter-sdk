import 'dart:io';
import 'dart:ui';

import 'package:package_info_plus/package_info_plus.dart';

import 'injector_util.dart';

class AuthUtil {
  AuthUtil._internal();

  static Future<String> buildRedirectUri() async {
    var packageName = await getPackageName();
    return '$packageName://autoauth';
  }

  static String buildAuthUrl(
    String authorizeUrl,
    String codeChallenge,
    String redirectUri,
    String clientId,
    Locale locale,
    String forceRegistration,
  ) {
    var amperWhat = authorizeUrl.contains('?') ? '&' : '?';
    return '$authorizeUrl${amperWhat}code_challenge=$codeChallenge&redirect_uri=$redirectUri&client_id=$clientId&locale=${locale.languageCode}&force_registration=$forceRegistration&code_challenge_method=S256&response_type=code&disclaimer=none';
  }

  static String buildWebLogoutUrl(
          String baseUrl, String token, String redirectUri) =>
      '$baseUrl/logout?token=$token&redirect_uri=$redirectUri';

  static Future<bool> isAuthDeepLinkValid(Uri uri) async {
    var packageName = await getPackageName();
    if (packageName != uri.scheme) return false;
    if ('autoauth' != uri.host) return false;
    return true;
  }

  static Future<String> getPackageName() async {
    if (Platform.isWindows) {
      return (await InjectorUtil().provideRepository().getPackageName())!;
    }
    return (await PackageInfo.fromPlatform()).packageName;
  }
}
