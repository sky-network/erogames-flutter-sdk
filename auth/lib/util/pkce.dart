import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';

class Pkce {
  static final Pkce _instance = Pkce._internal();

  String? _codeVerifier;

  factory Pkce() => _instance;

  Pkce._internal();

  String getCodeVerifier({bool generateNew = false}) {
    if (_codeVerifier == null || generateNew) {
      _codeVerifier = generateCodeVerifier();
    }
    return _codeVerifier!;
  }

  String generateCodeChallenge(String codeVerifier) =>
      base64UrlEncode(sha256.convert(ascii.encode(codeVerifier)).bytes)
          .split('=')[0];

  String generateCodeVerifier() {
    final random = Random.secure();
    final length = _random(43, 128);
    return base64UrlEncode(List.generate(length, (_) => random.nextInt(256)))
        .split('=')[0];
  }

  int _random(int min, int max) => min + Random().nextInt(max - min);
}
