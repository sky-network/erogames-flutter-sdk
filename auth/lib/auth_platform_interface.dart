import 'package:flutter/rendering.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'auth_method_channel.dart';
import 'model/token.dart';
import 'model/user.dart';
import 'model/whitelabel.dart';

typedef AuthCallback = Function(User? user, String? error);

abstract class AuthPlatform extends PlatformInterface {
  /// Constructs a ErogamesAuthPlatform.
  AuthPlatform() : super(token: _token);

  static final Object _token = Object();

  static AuthPlatform _instance = MethodChannelAuth();

  /// The default instance of [AuthPlatform] to use.
  ///
  /// Defaults to [MethodChannelAuth].
  static AuthPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [AuthPlatform] when
  /// they register themselves.
  static set instance(AuthPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<void> onAuth(AuthCallback authCallback) async =>
      throw UnimplementedError('onAuth() has not been implemented.');

  Future<void> init(String clientId, {String? packageName}) async =>
      throw UnimplementedError('init() has not been implemented.');

  Future<void> signup({Locale? locale}) async =>
      throw UnimplementedError('signup() has not been implemented.');

  Future<void> login({Locale? locale}) async =>
      throw UnimplementedError('login() has not been implemented.');

  Future<void> logout({bool webLogout = false}) async =>
      throw UnimplementedError('logout() has not been implemented.');

  Future<User?> getUser() async =>
      throw UnimplementedError('getUser() has not been implemented.');

  Future<void> reloadUser() async =>
      throw UnimplementedError('reloadUser() has not been implemented.');

  Future<Token?> getToken() async =>
      throw UnimplementedError('getToken() has not been implemented.');

  Future<void> refreshToken() async =>
      throw UnimplementedError('refreshToken() has not been implemented.');

  Future<Whitelabel?> getWhitelabel() async =>
      throw UnimplementedError('getWhitelabel() has not been implemented.');

  Future<void> loadWhitelabel() async =>
      throw UnimplementedError('loadWhitelabel() has not been implemented.');
}
