import 'dart:convert';

class Whitelabel {
  final String? slug;
  final String? name;
  final String? url;
  final String? logoUrl;
  final String? darkBackgroundLogoUrl;
  final String? darkBackgroundLogoSvgUrl;
  final String? brightBackgroundLogoUrl;
  final String? brightBackgroundLogoSvgUrl;
  final String? authorizeUrl;
  final String? tokenUrl;
  final String? profileUrl;
  final Map<String, String> buyErogoldUrl;
  final Map<String, String> supportUrl;
  final Map<String, String> tosUrl;

  Whitelabel(
      this.slug,
      this.name,
      this.url,
      this.logoUrl,
      this.darkBackgroundLogoUrl,
      this.darkBackgroundLogoSvgUrl,
      this.brightBackgroundLogoUrl,
      this.brightBackgroundLogoSvgUrl,
      this.authorizeUrl,
      this.tokenUrl,
      this.profileUrl,
      this.buyErogoldUrl,
      this.supportUrl,
      this.tosUrl);

  Map<String, dynamic> toJson() => {
        'slug': slug,
        'name': name,
        'url': url,
        'logo_url': logoUrl,
        'dark_background_logo_url': darkBackgroundLogoUrl,
        'dark_background_logo_svg_url': darkBackgroundLogoSvgUrl,
        'bright_background_logo_url': brightBackgroundLogoUrl,
        'bright_background_logo_svg_url': brightBackgroundLogoSvgUrl,
        'authorize_url': authorizeUrl,
        'token_url': tokenUrl,
        'profile_url': profileUrl,
        'buy_erogold_url': buyErogoldUrl,
        'support_url': supportUrl,
        'tos_url': tosUrl,
      };

  Whitelabel.fromJson(Map<String, dynamic> jsonMap)
      : slug = jsonMap['slug'],
        name = jsonMap['name'],
        url = jsonMap['url'],
        logoUrl = jsonMap['logo_url'],
        darkBackgroundLogoUrl = jsonMap['dark_background_logo_url'],
        darkBackgroundLogoSvgUrl = jsonMap['dark_background_logo_svg_url'],
        brightBackgroundLogoUrl = jsonMap['bright_background_logo_url'],
        brightBackgroundLogoSvgUrl = jsonMap['bright_background_logo_svg_url'],
        authorizeUrl = jsonMap['authorize_url'],
        tokenUrl = jsonMap['token_url'],
        profileUrl = jsonMap['profile_url'],
        buyErogoldUrl = Map<String, String>.from(jsonMap['buy_erogold_url']),
        supportUrl = Map<String, String>.from(jsonMap['support_url']),
        tosUrl = Map<String, String>.from(jsonMap['tos_url']);

  static Whitelabel fromString(String whitelabelStr) =>
      Whitelabel.fromJson(json.decode(whitelabelStr) as Map<String, dynamic>);
}
