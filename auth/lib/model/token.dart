import 'dart:convert';

class Token {
  final String? accessToken;
  final String? refreshToken;
  final String? tokenType;
  final String? scope;
  final int expiresIn;
  final int createdAt;

  Token(this.accessToken, this.refreshToken, this.tokenType, this.scope,
      this.expiresIn, this.createdAt);

  Map<String, dynamic> toJson() => {
        'access_token': accessToken,
        'refresh_token': refreshToken,
        'tokenType': tokenType,
        'scope': scope,
        'expires_in': expiresIn,
        'created_at': createdAt,
      };

  Token.fromJson(Map<String, dynamic> json)
      : accessToken = json['access_token'],
        refreshToken = json['refresh_token'],
        tokenType = json['tokenType'],
        scope = json['scope'],
        expiresIn = json['expires_in'] ?? 0,
        createdAt = json['created_at'] ?? 0;

  static Token fromString(String tokenStr) =>
      Token.fromJson(json.decode(tokenStr) as Map<String, dynamic>);

  bool isExpired() {
    double timestamp = DateTime.now().millisecondsSinceEpoch / 1000;
    return timestamp > (createdAt + expiresIn).toDouble();
  }
}
