import 'dart:convert';

class User {
  final int id;
  final String? email;
  final String? username;
  final String? avatarUrl;
  final String? language;
  final int? balance;

  User(this.id, this.email, this.username, this.avatarUrl, this.language,
      this.balance);

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': email,
        'username': username,
        'avatar_url': avatarUrl,
        'language': language,
        'balance': balance,
      };

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        username = json['username'],
        avatarUrl = json['avatar_url'],
        language = json['language'],
        balance = json['balance'];

  static User fromString(String userStr) =>
      User.fromJson(json.decode(userStr) as Map<String, dynamic>);
}
