import 'package:dio/dio.dart';

import '../model/token.dart';
import '../model/user.dart';
import '../model/whitelabel.dart';

class ApiService {
  static const String _baseUrl = 'https://erogames.com/';
  static const String _jwtTokenEndpoint = 'api/v1/authenticate';
  static const String _whitelabelsEndpoint = 'api/v1/whitelabels';
  static const String _xSdkVersion = '1.1';

  Dio _getDio() {
    var dio = Dio();
    // dio.interceptors.add(PrettyDioLogger(
    //   requestBody: true
    // ));
    dio.options.headers["X-SDK-VERSION"] = _xSdkVersion;
    return dio;
  }

  Future<String> getJwtToken(String? accessKey) async {
    var data = {'access_key': accessKey};
    var response =
        await _getDio().post('$_baseUrl$_jwtTokenEndpoint', data: data);
    return response.data['token'];
  }

  Future<List<Whitelabel>> getWhitelabels(String? jwtToken) async {
    var dio = _getDio();
    dio.options.headers["Authorization"] = jwtToken;
    var response = await dio.get('$_baseUrl$_whitelabelsEndpoint');

    var whitelabelList = response.data['whitelabels'] as List<dynamic>;
    return whitelabelList.map((e) => Whitelabel.fromJson(e)).toList();
  }

  Future<User> getUser(String profileUrl, String? accessToken) async {
    var dio = _getDio();
    dio.options.headers["Authorization"] = accessToken;
    var response = await dio.get(profileUrl);
    return User.fromJson(response.data['user']);
  }

  Future<Token> getToken(String tokenUrl, String? clientId, String? code,
      String? redirectUri, String codeVerifier,
      {String grantType = "authorization_code"}) async {
    var data = {
      'client_id': clientId,
      'code': code,
      'grant_type': grantType,
      'redirect_uri': redirectUri,
      'code_verifier': codeVerifier,
    };
    var response = await _getDio().post(tokenUrl, data: data);
    return Token.fromJson(response.data);
  }

  Future<Token> refreshToken(
      String tokenUrl, String? clientId, String? refreshToken,
      {String? grantType = "refresh_token"}) async {
    var data = {
      'client_id': clientId,
      'grant_type': grantType,
      'refresh_token': refreshToken,
    };
    var response = await _getDio().post(tokenUrl, data: data);
    return Token.fromJson(response.data);
  }
}
