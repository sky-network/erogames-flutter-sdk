import 'dart:convert';
import 'dart:ui';

import '../model/token.dart';
import '../model/user.dart';
import '../model/whitelabel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  static const String _prefUser = "com_erogames_flutter_auth_pref_user";
  static const String _prefToken = "com_erogames_flutter_auth_pref_token";
  static const String _prefWhitelabel =
      "com_erogames_flutter_auth_pref_whitelabel";
  static const String _prefLocale = "com_erogames_flutter_auth_pref_locale";
  static const String _prefClientId =
      "com_erogames_flutter_auth_pref_client_id";
  static const String _prefWhitelabelId =
      "com_erogames_flutter_auth_pref_whitelabel_id";
  static const String _prefAccessKey =
      "com_erogames_flutter_auth_pref_access_key";
  static const String _prefPackageName =
      "com_erogames_flutter_auth_pref_package_name";

  SharedPreferences? _prefs;

  Future<SharedPreferences> _getPrefs() async {
    _prefs ??= await SharedPreferences.getInstance();
    return _prefs!;
  }

  Future<void> setUser(User user) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefUser, jsonEncode(user));
  }

  Future<User?> getUser() async {
    var prefs = await _getPrefs();
    String? userStr = prefs.getString(_prefUser);
    if (userStr == null || userStr.isEmpty) return null;
    return User.fromJson(jsonDecode(userStr));
  }

  Future<void> removeUser() async {
    var prefs = await _getPrefs();
    await prefs.remove(_prefUser);
  }

  Future<void> setToken(Token token) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefToken, jsonEncode(token));
  }

  Future<Token?> getToken() async {
    var prefs = await _getPrefs();
    String? tokenStr = prefs.getString(_prefToken);
    if (tokenStr == null || tokenStr.isEmpty) return null;
    return Token.fromJson(jsonDecode(tokenStr));
  }

  Future<void> removeToken() async {
    var prefs = await _getPrefs();
    await prefs.remove(_prefToken);
  }

  Future<void> setLocale(Locale locale) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefLocale, locale.languageCode);
  }

  Future<Locale> getLocale() async {
    var prefs = await _getPrefs();
    String? languageCode = prefs.getString(_prefLocale);
    if (languageCode == null || languageCode.isEmpty) return const Locale('en');
    return Locale(languageCode);
  }

  Future<void> setWhitelabel(Whitelabel whitelabel) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefWhitelabel, jsonEncode(whitelabel));
  }

  Future<Whitelabel?> getWhitelabel() async {
    var prefs = await _getPrefs();
    String? wlStr = prefs.getString(_prefWhitelabel);
    if (wlStr == null || wlStr.isEmpty) return null;
    return Whitelabel.fromJson(jsonDecode(wlStr));
  }

  Future<void> removeWhitelabel() async {
    var prefs = await _getPrefs();
    await prefs.remove(_prefWhitelabel);
  }

  Future<void> setWhitelabelId(String whitelabelId) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefWhitelabelId, whitelabelId);
  }

  Future<String> getWhitelabelId() async {
    var prefs = await _getPrefs();
    return prefs.getString(_prefWhitelabelId)!;
  }

  Future<void> setClientId(String clientId) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefClientId, clientId);
  }

  Future<String> getClientId() async {
    var prefs = await _getPrefs();
    return prefs.getString(_prefClientId)!;
  }

  Future<void> setAccessKey(String accessKey) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefAccessKey, accessKey);
  }

  Future<String> getAccessKey() async {
    var prefs = await _getPrefs();
    return prefs.getString(_prefAccessKey)!;
  }

  Future<void> setPackageName(String packageName) async {
    var prefs = await _getPrefs();
    await prefs.setString(_prefPackageName, packageName);
  }

  Future<String?> getPackageName() async {
    var prefs = await _getPrefs();
    return prefs.getString(_prefPackageName);
  }
}
