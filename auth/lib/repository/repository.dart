import 'dart:ui';

import '../api/api_service.dart';
import '../model/token.dart';
import '../model/user.dart';
import '../model/whitelabel.dart';
import '../util/constants.dart';
import 'storage.dart';
import 'package:collection/collection.dart';

class Repository {
  final ApiService _apiService;
  final Storage _storage;

  Repository(
      String accessKey, String whitelabelId, this._apiService, this._storage) {
    _storage.setAccessKey(accessKey);
    _storage.setWhitelabelId(whitelabelId);
  }

  Future<String> loadJwtToken(String accessKey) async =>
      await _apiService.getJwtToken(accessKey);

  Future<void> setClientId(String clientId) async =>
      await _storage.setClientId(clientId);

  Future<String> getClientId() async => await _storage.getClientId();

  Future<String> getAccessKey() async => await _storage.getAccessKey();

  Future<String> getWhitelabelId() async => await _storage.getWhitelabelId();

  Future<List<Whitelabel>> _loadWhitelabels() async {
    var jwtToken = await loadJwtToken(await getAccessKey());
    return await _apiService.getWhitelabels(jwtToken);
  }

  Future<void> setWhitelabel(Whitelabel whitelabel) async =>
      await _storage.setWhitelabel(whitelabel);

  Future<Whitelabel?> getWhitelabel() async => await _storage.getWhitelabel();

  Future<Whitelabel> getOrLoadWhitelabel() async =>
      await _storage.getWhitelabel() ?? await loadWhitelabel();

  Future<Whitelabel> loadWhitelabel() async {
    var whitelabelId = await getWhitelabelId();
    var whitelabels = await _loadWhitelabels();
    var whitelabel =
        whitelabels.firstWhereOrNull((e) => e.slug == whitelabelId);
    if (whitelabel == null) {
      throw ('The whitelabel $whitelabelId does not exist.');
    }
    await setWhitelabel(whitelabel);
    return whitelabel;
  }

  Future<void> removeWhitelabel() async => await _storage.removeWhitelabel();

  Future<void> setToken(Token token) async => _storage.setToken(token);

  Future<Token?> getToken() async => await _storage.getToken();

  Future<Token?> getOrRefreshToken() async {
    var token = await _storage.getToken();
    if (token != null && token.isExpired()) return await refreshToken();
    return token;
  }

  Future<Token> loadToken(String? code, String grantType, String? redirectUri,
      String codeVerifier) async {
    var whitelabel = await getWhitelabel();
    if (whitelabel == null) throw ('The whitelabel can\'t be null.');
    var token = await _apiService.getToken(whitelabel.tokenUrl!,
        await getClientId(), code, redirectUri, codeVerifier,
        grantType: grantType);
    await setToken(token);
    return token;
  }

  Future<Token> refreshToken() async {
    var token = await getToken();
    var whitelabel = await getWhitelabel();
    if (token == null) throw ('The token can\'t be null.');
    if (whitelabel == null) throw ('The whitelabel can\'t be null.');

    var refreshedToken = await _apiService.refreshToken(
        whitelabel.tokenUrl!, await getClientId(), token.refreshToken);
    await setToken(refreshedToken);
    return refreshedToken;
  }

  Future<void> removeToken() async => await _storage.removeToken();

  Future<void> setUser(User user) async => await _storage.setUser(user);

  Future<User?> getUser() async => await _storage.getUser();

  Future<User> loadUser({String? profileUrl}) async {
    var token = await getOrRefreshToken();
    if (token == null) throw ('The token can\'t be null.');

    Whitelabel? whitelabel;
    if (profileUrl == null) {
      whitelabel = await getWhitelabel();
      if (whitelabel == null) throw ('The whitelabel can\'t be null.');
    }

    var user = await _apiService.getUser(
        profileUrl ?? whitelabel!.profileUrl!, "Bearer ${token.accessToken}");
    await setUser(user);
    return user;
  }

  Future<User> reloadUser() async {
    var token = await getOrRefreshToken();
    var whitelabel = await getWhitelabel();
    if (token == null || whitelabel == null) {
      throw ('The token and/or whitelabel can\'t be null.');
    }

    var user = await _apiService.getUser(
        whitelabel.profileUrl!, "Bearer ${token.accessToken}");
    await setUser(user);
    return user;
  }

  Future<void> removeUser() async => await _storage.removeUser();

  Future<void> setLocale(Locale locale) async {
    if (Constants.supportedLanguages.contains(locale.languageCode)) {
      await _storage.setLocale(locale);
    } else {
      await _storage.setLocale(const Locale('en'));
    }
  }

  Future<Locale> getLocale() async => await _storage.getLocale();

  Future<void> setPackageName(String packageName) async =>
      await _storage.setPackageName(packageName);

  Future<String?> getPackageName() async => await _storage.getPackageName();

  Future<void> clearLocalData() async {
    await removeUser();
    await removeToken();
    await removeWhitelabel();
  }
}
