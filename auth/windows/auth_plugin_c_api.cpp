#include "include/erogames_auth/auth_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "auth_plugin.h"

void AuthPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  auth::AuthPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
