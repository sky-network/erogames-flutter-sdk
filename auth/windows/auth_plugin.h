#ifndef FLUTTER_PLUGIN_AUTH_PLUGIN_H_
#define FLUTTER_PLUGIN_AUTH_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

namespace auth {

class AuthPlugin : public flutter::Plugin {
 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrarWindows *registrar);

  AuthPlugin();

  virtual ~AuthPlugin();

  // Disallow copy and assign.
  AuthPlugin(const AuthPlugin&) = delete;
  AuthPlugin& operator=(const AuthPlugin&) = delete;

 private:
  // Called when a method is called on this plugin's channel from Dart.
  void HandleMethodCall(
      const flutter::MethodCall<flutter::EncodableValue> &method_call,
      std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);
};

}  // namespace auth

#endif  // FLUTTER_PLUGIN_AUTH_PLUGIN_H_
