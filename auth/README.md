# Erogames Auth Plugin

The plugin for user authentication via Erogames API (for macOS and Android only).  
[Android Auth Library](https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/auth)

## Get started

To add this plugin to the project add the reference to the `pubspec.yaml` file:

```dart
...
dependencies:
  erogames_auth:
    git:
      url: git@gitlab.com:sky-network/erogames-flutter-sdk.git
      path: auth
      ref: auth-v<x.y.z>
...
```

**To set authentication provider (whitelabel) and client ID, you need to add the following strings (strings.xml):**

```xml
<resources>
    ...
    <string name="com_erogames_sdk_whitelabel_id">your_whitelabel_id</string>
    <string name="com_erogames_sdk_client_id">your_client_id</string>
    ...
</resources>
```

### Import package
```dart
import 'package:erogames_auth/auth.dart';
```

### Initialize
```dart
ErogamesAuth.init(clientId);
ErogamesAuth.onAuth((authCallback) {
  // Listener will trigger
  // once the user has logged in, signed up or logged out
});
```

### Log in:
```dart
ErogamesAuth.login();
```

### Sign up:
```dart
ErogamesAuth.signup();
```

### Log out case 1:
```dart
// The local data only will be cleaned
ErogamesAuth.logout();
```

### Log out case 2:
```dart
// The user will log out of the web site and
// the local data will be cleaned as well
ErogamesAuth.logout(webLogout: true);
```

### Get current user:
```dart
ErogamesAuth.getUser();
```

### Reload user:
```dart
ErogamesAuth.reloadUser();
```

### Get token:
```dart
ErogamesAuth.getToken();
```

### Refresh token:
```dart
ErogamesAuth.refreshToken();
```