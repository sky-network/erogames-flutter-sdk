# Version 2.2.2 - Jul 14, 2023
* Updated project dependencies.

# Version 2.2.1 - Feb 1, 2023
* Fixed url protocol handling on macOS.

# Version 2.2.0 - Jan 31, 2023
* Added Windows support.

# Version 2.1.1 - Jan 12, 2023
* Fixed whitelabel switching (macOS).

# Version 2.1.0 - Jan 11, 2023
* Added macOS support.

# Version 2.0.0 - Dec 14, 2022
* Updated project dependencies.
* Removed deprecated code.

# Version 1.4.5 - Oct 26, 2022
* Upgrade AGP.

# Version 1.4.4 - Jun 13, 2021
* Bug fixes.

# Version 1.4.3 - Jun 12, 2021
## Removed
* An ability to add data points.

# Version 1.4.2 - May 15, 2021
## Added
* An ability to get payment information.

