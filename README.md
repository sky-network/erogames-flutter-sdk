# Erogames Flutter SDK

...

## Erogames Auth Plugin

The plugin for user authentication via Erogames API. Supports Android only.
[README](https://gitlab.com/sky-network/erogames-flutter-sdk/-/tree/master/auth)

## Erogames Analytics Plugin

[README](https://gitlab.com/sky-network/erogames-flutter-sdk/-/tree/master/analytics)
